﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Korrutamine
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] tagasiside =
                {
                    "Proovi veel: "
                    , "Põmmpea! Proovi uuesti: "
                    , "No kesse nii loll on! Proovi veelkord: "
                    , "Pigi peab p..sse minema! Katseta veel: "
                    , "Loll on ikka loll - niisiis teine kord"
                };
            Random r = new Random();
            int vastuseid = 0;
            int küsimusi = 0;
            do
            {
                int a = r.Next() % 10 + 1;
                int b = r.Next() % 10 + 1;
                küsimusi++;
                Console.WriteLine($"arvuta palju on {a} x {b}");
                Console.Write("Mis sa vastad: ");

                for (int vastus = 0; vastus < tagasiside.Length; vastus++) 
                {
                    int korrutis;
              
                    while (! int.TryParse(Console.ReadLine(), out korrutis)) Console.WriteLine("Korralik vastus palun:");
                    if (korrutis == a * b) { vastuseid++; Console.WriteLine("Üllatavalt õige vastus"); vastus = tagasiside.Length; }
                    #region MyRegion
                    //else switch (vastus)
                    //    {
                    //        case 0: Console.Write("proovi uuesti: "); break;
                    //        case 1: Console.Write("põmmpea oled või? Proovi veel: "); break;
                    //        case 2: Console.Write("no ma ei või ... PRoovi veel: "); break;
                    //        case 3: Console.Write("oehh... Proovi ikka: "); break;
                    //        case 4: Console.WriteLine("aitab proovimisest - jääd lolliks"); break;
                    //    }

                    #endregion
                    else if (vastus < tagasiside.Length) Console.WriteLine(tagasiside[vastus]);
                }
                Console.Write("Kas aitab : ");
            } while (! (Console.ReadLine() == "jah"));

            Console.WriteLine($"nonii - said kokku {vastuseid} õiget {küsimusi}-st") ;
        }
    }
}
